﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using apex;
using moran;

public partial class _Default : System.Web.UI.Page 
{
    private SforceService enterpriseBinding = new SforceService();
    private moran_ServiceService myBinding = new moran_ServiceService();

    protected void Page_Load(object sender, EventArgs e)
    {
        LoginResult lr = enterpriseBinding.login("yourUserName", "Password+token");
        myBinding.SessionHeaderValue = new moran.SessionHeader();
        myBinding.SessionHeaderValue.sessionId = lr.sessionId;

        int idx1 = lr.serverUrl.IndexOf(@"/services/");
        int idx2 = myBinding.Url.IndexOf(@"/services/");
        if (idx1 == 0 || idx2 == 0)
        {
            Response.Write("Invalid URL strings in bindings");
            return;
        }

        myBinding.Url = lr.serverUrl.Substring(0, idx1) + myBinding.Url.Substring(idx2);
        
        // End update
        string result = myBinding.createContact("EdgarC#","FROM ASPX");
        Label1.Text = "Contact created with ID:" + result;
    }
}
